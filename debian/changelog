ap-utils (1.5-6) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 25 May 2022 14:39:22 +0100

ap-utils (1.5-5) unstable; urgency=medium

  * QA upload.
  * debian/control:
    + Bump debhelper compat to v13.
  * debian/rules: Use dh13 syntax.
  * debian/README.source: Dropped, useless.

  [ Lukas Märdian ]
  * Adopt autotools for required 'external' parameter of AM_GNU_GETTEXT
    (Closes: #978372)

 -- Boyuan Yang <byang@debian.org>  Tue, 21 Sep 2021 19:23:58 -0400

ap-utils (1.5-4) unstable; urgency=medium

  * QA upload.
  * debian/control:
    + Bump debhelper compat to v12.
    + Bump Standards-Version to 4.5.0.
    + Update Vcs-* fields to use git packaging repo under Salsa
      Debian group.
  * debian/rules:
    + Rewrite using dh sequencer.
  * debian/source/format: Use "3.0 (quilt)" format.

 -- Boyuan Yang <byang@debian.org>  Sat, 04 Apr 2020 21:13:38 -0400

ap-utils (1.5-3) unstable; urgency=medium

  * QA upload.
  * Add a debian/watch file.
  * Bump Standards-Version to 3.9.8.
  * Set Debian QA Group as maintainer. (see #703278)
  * Run wrap-and-sort.

 -- Allan Dixon Jr. <allandixonjr@gmail.com>  Sun, 06 Nov 2016 14:48:50 -0500

ap-utils (1.5-2) unstable; urgency=low

  * Update config.{sub,guess} in the right place at build time -
    closes: #534825

 -- Ben Hutchings <ben@decadent.org.uk>  Wed, 26 Aug 2009 00:43:16 +0100

ap-utils (1.5-1) unstable; urgency=low

  * New maintainer - closes: #519617
  * Newer upstream release
  * Upgrade to debhelper 7
  * Install Ukrainian README file as README.uk and English as README -
    closes: #291898
    - Convert both files from KOI8-U to UTF-8 encoding
  * Use quilt for patches
  * Fix spelling errors in manual pages, thanks to A. Costa -
    closes: #461807, #461808, #461810, #461811, #461812
  * Update Standards-Version to 3.8.1
    - Add Homepage field

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 19 Apr 2009 04:06:50 +0100

ap-utils (1.4.1+1.5pre1-1) unstable; urgency=low

  * The "Freedom Of Ukraine" release
  * New upstream release (closes: #243208 fix little incorrection in man page)
    - added two new programs ap-auth and ap-gl
    - added support for VERNET AP extension
    - lots of ui improvement
    - Some lintian fixes about utf8 encoding

 -- Celso González <celso@bulma.net>  Sun,  5 Dec 2004 19:40:10 +0100

ap-utils (1.4.1-1) unstable; urgency=low

  * New upstream release (closes: #234310)
    - Decreases time response in snmp
    - many bugfixes
    - Updated french and ukranian traslations

 -- Celso González <celso@bulma.net>  Fri, 18 Jun 2004 12:10:00 +0200

ap-utils (1.3.3-1) unstable; urgency=low

  * New upstream release including this features
    -many new options and better support for ATMEL12350 devices;
    -added AP type autodetection
    -added option to choose AP to connect from AP search result;
    -user interface improvements;
    -many internal improvements to avoid memory leaks;
    -BSD compile fixes;
    -bigendian fixes;
    -added Romanian and Dutch translation
  * Fix lintian warning about Ukranian doc install
  * Bump standards to 3.6.1.0 (all control files are now in utf8)

 -- Celso González <celso@bulma.net>  Mon, 27 Oct 2003 12:52:52 +0100

ap-utils (1.3.2-1) unstable; urgency=low

  * New upstream release (closes: #182096 problems with 64bits machines)

 -- Celso González <celso@bulma.net>  Wed, 28 May 2003 12:27:32 +0200

ap-utils (1.3.1-2) unstable; urgency=low

  * Bump standards version to 3.5.10, no changes needed
  * Fix lintian warnings aboutautotools outdated (solves mips
    and mipsel compilation)
  * Fix libncurses-dev depends to libncurses5-dev
  * Add Ovislink and Ukrainian documentation in doc

 -- Celso González <celso@bulma.net>  Thu, 22 May 2003 11:16:30 +0200

ap-utils (1.3.1-1) unstable; urgency=low

  * New upstrem version (closes: #174673 and #186483)
  * Adopted package by new maintainer (closes: #193116)
  * Remove references to ap-atmel and ap-nwm in control file
  * Updated standards version to 3.5.9.0
  * Fix lintian complaint about versioned debhelper

 -- Celso González <celso@bulma.net>  Mon, 19 May 2003 11:38:29 +0200

ap-utils (1.1-1) unstable; urgency=low

  * New upstream version.
  * Updated standards version to 3.5.7.

 -- Robert McQueen <robot101@debian.org>  Sun, 29 Sep 2002 23:06:33 +0100

ap-utils (1.0.4-1) unstable; urgency=low

  * New upstream version.
  * Includes manpages.

 -- Robert McQueen <robot101@debian.org>  Sat, 10 Aug 2002 13:53:27 +0100

ap-utils (1.0-020624-1) unstable; urgency=low

  * Initial release.					(closes: #150970)

 -- Robert McQueen <robot101@debian.org>  Tue, 25 Jun 2002 18:32:02 +0100
