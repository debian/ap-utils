/*
 *      latest.c from Access Point SNMP Utils for Linux
 *
 * Copyright (c) 2002 Roman Festchook <roma at polesye dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 from
 * June 1991 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "ap-utils.h"

extern WINDOW *main_sub;

#define OID_NUM 8
#define DESCR _("Reason: %u Station: %02X%02X%02X%02X%02X%02X")

void latest()
{
    char oid_dot11DisassociateReason[] =
	{ 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x01, 0x01, 0x0f, 0x01 };
    char oid_dot11DisassociateStation[] =
	{ 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x01, 0x01, 0x10, 0x01 };
    char oid_dot11DeauthenticateReason[] =
	{ 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x01, 0x01, 0x11, 0x01 };
    char oid_dot11DeauthenticateStation[] =
	{ 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x01, 0x01, 0x12, 0x01 };
    char oid_dot11AuthenticateFailStatus[] =
	{ 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x01, 0x01, 0x13, 0x01 };
    char oid_dot11AuthenticateFailStation[] =
	{ 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x01, 0x01, 0x14, 0x01 };
    char oid_secLastError[] =
	{ 0x2b, 0x06, 0x01, 0x04, 0x01, 0x87, 0x29, 0x03, 0x01, 0x03, 0x02,
	0x03, 0x00
    };
    char oid_secLastErrorAddress[] =
	{ 0x2b, 0x06, 0x01, 0x04, 0x01, 0x87, 0x29, 0x03, 0x01, 0x03, 0x02,
	0x04, 0x00
    };
    char message[1024];

    int i;
    varbind varbinds[OID_NUM];
    noecho();
    print_help(WAIT_RET);

    for (i = 0; i < OID_NUM; i++) {
	varbinds[i].value = oid_dot11DisassociateReason;
	varbinds[i].len_val = 0;
	varbinds[i].type = NULL_VALUE;
    }
    varbinds[0].oid = oid_dot11DisassociateReason;
    varbinds[0].len_oid = sizeof(oid_dot11DisassociateReason);
    varbinds[1].oid = oid_dot11DisassociateStation;
    varbinds[1].len_oid = sizeof(oid_dot11DisassociateStation);
    varbinds[2].oid = oid_dot11DeauthenticateReason;
    varbinds[2].len_oid = sizeof(oid_dot11DeauthenticateReason);
    varbinds[3].oid = oid_dot11DeauthenticateStation;
    varbinds[3].len_oid = sizeof(oid_dot11DeauthenticateStation);
    varbinds[4].oid = oid_dot11AuthenticateFailStatus;
    varbinds[4].len_oid = sizeof(oid_dot11AuthenticateFailStatus);
    varbinds[5].oid = oid_dot11AuthenticateFailStation;
    varbinds[5].len_oid = sizeof(oid_dot11AuthenticateFailStation);
    varbinds[6].oid = oid_secLastError;
    varbinds[6].len_oid = sizeof(oid_secLastError);
    varbinds[7].oid = oid_secLastErrorAddress;
    varbinds[7].len_oid = sizeof(oid_secLastErrorAddress);

    if (snmp(varbinds, OID_NUM, GET) < OID_NUM) {
	print_help(ERR_RET);
	goto exit;
    }

    print_top(NULL, _("Latest Events"));
    mvwaddstr(main_sub, 1, 0, _("Disassociate:"));
    sprintf(message, DESCR,
	    *varbinds[0].value,
	    varbinds[1].value[0] & 0xFF, varbinds[1].value[1] & 0xFF,
	    varbinds[1].value[2] & 0xFF, varbinds[1].value[3] & 0xFF,
	    varbinds[1].value[4] & 0xFF, varbinds[1].value[5] & 0xFF);
    mvwaddstr(main_sub, 2, 0, message);
    mvwaddstr(main_sub, 4, 0, _("Deauthenticate:"));
    sprintf(message, DESCR,
	    *varbinds[2].value,
	    varbinds[3].value[0] & 0xFF, varbinds[3].value[1] & 0xFF,
	    varbinds[3].value[2] & 0xFF, varbinds[3].value[3] & 0xFF,
	    varbinds[3].value[4] & 0xFF, varbinds[3].value[5] & 0xFF);
    mvwaddstr(main_sub, 5, 0, message);
    mvwaddstr(main_sub, 7, 0, _("Authenticate Fail:"));
    sprintf(message, DESCR, *varbinds[4].value,
	    varbinds[5].value[0] & 0xFF, varbinds[5].value[1] & 0xFF,
	    varbinds[5].value[2] & 0xFF, varbinds[5].value[3] & 0xFF,
	    varbinds[5].value[4] & 0xFF, varbinds[5].value[5] & 0xFF);
    mvwaddstr(main_sub, 8, 0, message);
    mvwaddstr(main_sub, 10, 0, _("Last error:"));
    mvwaddstr(main_sub, 11, 0, _("Error:"));
    i = 0;
    while (varbinds[6].len_val > i && *(varbinds[6].value + i))
	mvwaddch(main_sub, 11, i + 15, *(varbinds[6].value + i++));

    sprintf(message, "%s%02X%02X%02X%02X%02X%02X", MAC,
	    varbinds[7].value[0] & 0xFF, varbinds[7].value[1] & 0xFF,
	    varbinds[7].value[2] & 0xFF, varbinds[7].value[3] & 0xFF,
	    varbinds[7].value[4] & 0xFF, varbinds[7].value[5] & 0xFF);
    mvwaddstr(main_sub, 12, 0, message);

    wrefresh(main_sub);

    print_help(ANY_KEY);
  exit:
    getch();
    print_top(NULL, NULL);
    clear_main(0);
}
