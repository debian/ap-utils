/*
 *      wlan.c from Access Point SNMP Utils for Linux
 *
 * Copyright (c) 2002 Roman Festchook <roma at polesye dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 from
 * June 1991 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

#include <stdlib.h>
#include <unistd.h>
#include "ap-utils.h"
#include <menu.h>

#define RATES_RECORD "[%d]   %02.1fM\t%s"

void advanced()
{
	char smtStationDBTimeout[] = { 0x2b, 0x06, 0x01, 0x04, 0x01, 0x87, 0x29, 0x03, 0x01, 0x02, 0x01, 0x0F, 0x00 };
	char smtACKWindow[] = { 0x2b, 0x06, 0x01, 0x04, 0x01, 0x87, 0x29, 0x03, 0x01, 0x02, 0x01, 22, 0x00 };

    char m_dbtimeout=0, m_ackwindow=0, message[80], ackwindow;
    unsigned int dbtimeout;
    int i;
    extern WINDOW *main_sub;
    varbind varbinds[2];

    for (i = 0; i < 2; i++) {
	varbinds[i].len_val = 0;
	varbinds[i].type = NULL_VALUE;
    }
    varbinds[0].oid = smtStationDBTimeout;
    varbinds[0].len_oid = sizeof(smtStationDBTimeout);
    varbinds[1].oid = smtACKWindow;
    varbinds[1].len_oid = sizeof(smtACKWindow);
    print_help(WAIT_RET);
    if (snmp(varbinds, 2, GET) < 2) {
	print_helperr(ERR_RET);
	goto exit;
    }

    if (varbinds[0].len_val == 2)
    	dbtimeout = varbinds[0].value[0]*256 + varbinds[0].value[1];
    else
	dbtimeout = *(varbinds[0].value);
	
    ackwindow = *(varbinds[1].value);

    sprintf(message, _("[D] DB Station Timeout: %d"), dbtimeout);
    mvwaddstr(main_sub, 1, 0, message);
    sprintf(message, _("[A] ACK Window: %d"), ackwindow);
    mvwaddstr(main_sub, 2, 0, message);
    
    print_top(NULL, _("Advanced Options"));
    print_help(_("DA - options; W - write conf; Q - quit to menu"));
    wrefresh(main_sub);

    noecho();
    while (1) {
	switch (getch()) {
	case 'Q':
	case 'q':
	    goto quit;
	case 'D':
	case 'd':
	    get_value(message, 1, 24, 6, INT_STRING, 0, 0, NULL);
	    dbtimeout = atoi(message);
	    m_dbtimeout = 1;
	    continue;
        case 'A':
        case 'a':
            get_value(message, 2, 16, 4, INT_STRING, 0, 0, NULL);
            ackwindow = atoi(message);
            m_ackwindow = 1;
            continue;
	case 'w':
	case 'W':
	    i = 0;
	    if (m_dbtimeout) {
		if (dbtimeout / 256 >= 1) {
		     message[0] = (dbtimeout - dbtimeout % 256)/ 256;
		     message[1] = dbtimeout % 256;
		     m_dbtimeout = 2;
		}
	    	else {
			m_dbtimeout = 1;
			message[0] = dbtimeout;
		}	
			
		varbinds[i].oid = smtStationDBTimeout;
		varbinds[i].len_oid = sizeof(smtStationDBTimeout);
		varbinds[i].value = message;
		varbinds[i].len_val = m_dbtimeout;
		varbinds[i].type = INT_VALUE;
		i++;
	    }
	    if (m_ackwindow) {
		varbinds[i].oid = smtACKWindow;
		varbinds[i].len_oid = sizeof(smtACKWindow);
		varbinds[i].value = &ackwindow;
		varbinds[i].len_val = 1;
		varbinds[i].type = INT_VALUE;
		i++;
	    }	

	    print_help(WAIT_SET);
	    if (snmp(varbinds, i, SET) <= 0) {
		print_helperr(ERR_SET);
		    goto exit;
	    }
	    wbkgd(main_sub, A_NORMAL);
	    wrefresh(main_sub);
	    print_help(DONE_SET);
	    goto exit;
	default:
	    continue;
	}
    }

  exit:
    getch();
  quit:
    print_top(NULL, NULL);
    clear_main(0);
}
