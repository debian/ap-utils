/*
 *     reset.c from Access Point SNMP Utils for Linux
 *
 * Copyright (c) 2002 Roman Festchook <roma at polesye dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 from
 * June 1991 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <unistd.h>
#include "ap-utils.h"

int SysReset()
{
    char sysReset[] = { 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x01, 0x02, 0x00 }, enable = 1;
    varbind varbinds[1];
    char operESSID[] = { 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x01, 0x01, 0x09, 0x01 };
    extern short ap_type;

	if (ap_type == ATMEL12350) {
		sysReset[5] = 0xe0;
		sysReset[6] = 0x3e;
	}
    
	if(ap_type == ATMEL410 || ap_type == ATMEL12350){
    		varbinds[0].oid = sysReset;
    		varbinds[0].len_oid = sizeof(sysReset);
		varbinds[0].value = &enable;
		varbinds[0].len_val = 1;
		varbinds[0].type = INT_VALUE;
	}
	else {
		varbinds[0].len_val = 0;
		varbinds[0].type = NULL_VALUE;
		varbinds[0].oid = operESSID;
		varbinds[0].len_oid = sizeof(operESSID);
		if (snmp(varbinds, 1, GET) <= 0)
	       		return -1;
	       varbinds[0].type = STRING_VALUE;
	}

    if (snmp(varbinds, 1, SET) <= 0)
	return -1;
    else {
	if(ap_type==NWN)
	     	sleep(15);
	return 0;
    }
}

