/*
 *     cmd.c from Access Point SNMP Utils for Linux
 *
 * Copyright (c) 2002 Roman Festchook <roma at polesye dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 from
 * June 1991 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

#include <unistd.h>
#include "ap-utils.h"

extern WINDOW *main_sub;
extern short ap_type;

void defaults()
{
    char sysLoadDefaults[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01,
	0x01, 0x04, 0x00
    }, enable = 1;
    varbind varbinds[1];

    if (ap_type == ATMEL12350) {
	sysLoadDefaults[5] = 0xE0;
	sysLoadDefaults[6] = 0x3E;
    }

    print_top(NULL, _("Restore factory default configuration"));
    mvwaddstr(main_sub, 3, 2,
	      _
	      ("After restoring factory defaults your current configuration"));
    mvwaddstr(main_sub, 4, 2, _("will be lost."));
    mvwaddstr(main_sub, 6, 20, _("Do you want to continue? "));
    wrefresh(main_sub);

    if (help_ysn())
	goto quit;

    print_help(WAIT_SET);

    varbinds[0].oid = sysLoadDefaults;
    varbinds[0].len_oid = sizeof(sysLoadDefaults);
    varbinds[0].value = &enable;
    varbinds[0].len_val = 1;
    varbinds[0].type = INT_VALUE;
    if (snmp(varbinds, 1, SET) <= 0)
	print_helperr(ERR_SET);
    else
	print_help
	    (_
	     ("Factory default settings loaded. Press any key to continue."));
    getch();
  quit:
    print_top(NULL, NULL);
}

void reset()
{
    print_top(NULL, _("Reset Access Point"));
    if(ap_type == ATMEL410) 
    	mvwaddstr(main_sub, 3, 5,
	      _("By reset you'll lose all non-uploaded configuration."));
    mvwaddstr(main_sub, 5, 20, _("Do you want to continue? "));
    wrefresh(main_sub);

    if (help_ysn())
	goto quit;

    print_help(WAIT_SET);
    if (SysReset())
	print_helperr(ERR_SET);
    else
	print_help(_("Access Point reset. Press any key to continue."));
    getch();
  quit:
    print_top(NULL, NULL);
}

int SysUpload()
{
    char sysUpload[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x01, 0x06,
	0x00
    }, enable = 1;
    varbind varbinds[1];

    if (ap_type == ATMEL12350) {
	sysUpload[5] = 0xE0;
	sysUpload[6] = 0x3E;
    }

    varbinds[0].oid = sysUpload;
    varbinds[0].len_oid = sizeof(sysUpload);
    varbinds[0].value = &enable;
    varbinds[0].len_val = 1;
    varbinds[0].type = INT_VALUE;
    if (snmp(varbinds, 1, SET) <= 0)
	return -1;
    else
	return 0;
}

void upload()
{
    print_top(NULL, _("Upload configuration"));
    mvwaddstr(main_sub, 3, 2,
	_("You may need to upload the configuration only if you've"));
    mvwaddstr(main_sub, 4, 2,
	_("changed some option values before. Using this option may"));
    mvwaddstr(main_sub, 5, 2,
	_("cause loss of your current configuration."));
    mvwaddstr(main_sub, 7, 20, ("Do you want to continue? "));
    wrefresh(main_sub);

    if (help_ysn())
	goto quit;

    print_help(WAIT_SET);
    if (SysUpload())
	print_helperr(ERR_SET);
    else
	print_help
	    (_("Configuration uploaded. Press any key to continue."));
    getch();
  quit:
    print_top(NULL, NULL);
}

